= License Approval

This page describes what constitutes an allowed or not-allowed license
for Fedora.

== Overview

In order to meet the goal of creating a complete, general purpose
operating system exclusively from free and open source software, all
software and other content made available by Feodora must be under
licenses determined to be allowed in Fedora, with only limited,
conditional exceptions.

Fedora applies different license approval criteria for different
categories of material. The strictest standards apply to code
(corresponding to the general *allowed* status), while somewhat
relaxed standards apply to other categories: documentation, content,
fonts, and firmware.

The Fedora legal documentation includes lists of
xref:allowed-licenses.adoc[allowed] and
xref:not-allowed-licenses.adoc[not-allowed] licenses for Fedora. Those
lists are generated from the data maintained in the
https://gitlab.com/fedora/legal/fedora-license-data[Fedora License
Data] repository.

If a license is listed in the xref:allowed-licenses.adoc[allowed]
list, then it is allowed in Fedora for the relevant category, i.e.,
allowed (for all categories), allowed for fonts, etc.) as described
below.

If a license is listed in the
xref:not-allowed-licenses.adoc[not-allowed] list, then it has already
been reviewed and determined to be not-allowed in Fedora. In some cases, an exception 
to the designation of not-allowed has been recorded in the "usage" field shown in the 
xref:not-allowed-licenses.adoc[not-allowed] list and the 
https://gitlab.com/fedora/legal/fedora-license-data/-/tree/main/data[corresponding TOML file].
If you think
Fedora should make an exception to the designation of a license as
not-allowed (for example, a time-limited or package-specific tolerance
of the license), you can raise this in an issue in (preferably) the
https://gitlab.com/fedora/legal/fedora-license-data[Fedora License
Data] repository or on the Fedora legal mailing list.

If a license that covers something in Fedora, or in a package that has
been proposed for inclusion in Fedora Linux, is not listed on the
allowed and not-allowed lists, then it must be reviewed. Please follow
the xref:license-review-process.adoc[license review process].

Whether a license is allowed or not allowed for Fedora is ultimately
determined by the Fedora Council. Today, this decision is normally
delegated to members of the Red Hat legal team who are specialists in
free software / open source licensing and who are very familiar with
and supportive of Fedora's
https://docs.fedoraproject.org/en-US/project/[mission and
foundations].

Fedora has a uniform set of licensing standards that apply not only to
Fedora Linux packages but also to material that is not part of a
Fedora Linux package but has been created or built for Fedora by
Fedora contributors. One example is this Fedora legal
documentation. Another example is Copr repositories. In this
documentation we sometimes refer specifically to "Fedora Linux" but
context may make clear that we intend to cover non-packaged Fedora
material as well.

== Allowed Licenses

A license is allowed if Fedora determines that the license is a free
software / open source license. At a high level, the inquiry involves
determining whether the license provides software freedom, and
(equivalently) making sure that the license does not place burdens on
users' exercise of its permissions that are inconsistent with evolving
community norms and traditions around what is acceptable in a free
software / open source license.  See
xref:license-approval/#_background_to_fedora_licensing_policy[below]
for more on the history of Fedora licensing policy.

A license that is allowed can be used for anything in Fedora, not just
code.

== Allowed for Documentation

Fedora may designate a license as allowed for documentation if it
meets the criteria for allowed licenses and it is specifically
designed for documentation.

In addition, Fedora classifies the following licenses as allowed for
documentation even though they do not meet the criteria for allowed
licenses:

* Creative Commons Attribution 4.0 International Public License and
  its predecessor versions

* Creative Commons Attribution-ShareAlike 4.0 International Public
  License and its predecessor versions

* The GNU Free Documentation License version 2.1 and its predecessor
  versions

NOTE: The Open Publication License v1.0, which was historically
classified as "good" for documentation provided that no Section VI
"license options" are elected, is now classified as not-allowed, but a
usage note states that it is permitted if no "options" are elected.

== Allowed for Content

“Content” means any material that is _not_ code, documentation, fonts
or firmware. Here are some examples of content:

* graphic image files
* audio files
* nonfunctional data sets
* AppStream metainfo.xml files
* upstream-of-Fedora standards documents (such as IETF RFCs) 

Fedora may designate a license as allowed for content if it meets the
criteria for allowed licenses with the following exceptions:

* The license may restrict or prohibit modification
* The license may say that it does not cover patents or grant any patent licenses

TIP: When we say that a standards document is subject to the relaxed
license criteria for content, we are referring to the license that
covers the copyright-related permissions for the document itself, not
the technology specified by the document. It is conceivable that
Fedora might choose not to include a package containing an
implementation of a specification because the specification license
does not provide an adequate grant of patent rights. Note, however,
that man pages are considered documentation even when they are derived
from standards documents.

IMPORTANT: CC0, which has a clause that says that it does not license
or otherwise "affect" patent rights, is no longer allowed for code in
Fedora, but it continues to be allowed for content.

== Allowed for Fonts

Fedora may designate a license as allowed for fonts if it meets the
criteria for allowed licenses with the following exception:

* The license may contain a nominal prohibition on resale or distribution in isolation

A good example of what we mean by a "nominal prohibition on resale or
distribution in isolation" is found in the following condition in the
https://spdx.org/licenses/OFL-1.1.html[SIL Open Font License 1.1]:

`Neither the Font Software nor any of its individual components, in
Original or Modified Versions, may be sold by itself.`

This prohibition would not be acceptable in a license applicable to
code.

NOTE: The prohibition in the SIL OFL is "nominal" because in FOSS
legal culture it has occasionally been asserted that one can get
around it (and similar provisions in certain other licenses) by
bundling a trivial 'Hello world' program with the fonts (and there is
actually some reason to believe the authors of the SIL OFL intended
this). If we were to encounter a license that had such a provision but
we believed a 'Hello world' solution was not even arguably viable, we
might not consider the license allowable for fonts.

== Allowed for Firmware

Some applications, drivers, and hardware require binary firmware
images to boot Fedora Linux or function properly. Fedora permits
inclusion of these files as long as they meet the following license
and technical requirements:

=== License requirements for firmware

Fedora may designate a license as allowed for firmware if it meets the
criteria for allowed licenses with the following exceptions:

* The license may prohibit modification, reverse engineering,
  disassembly or decompilation

* The license may require that the firmware be used only in conjunction with specified hardware

* The license may require that the firmware be redistributed only as
  incorporated in the redistributor's product (or as a maintenance
  update for existing end users of the redistributor's product)

** This may be limited further to those products of the redistributor
   that support or contain the hardware associated with the licensed
   firmware

* The license may require a redistributor to pass on or impose
  conditions on users that are no more restrictive than those
  authorized by Fedora itself with respect to firmware licenses
  
=== Technical firmware requirements

While these technical requirements for firmware have nothing to do
with licensing, they are included here for convenience.

* The files must be non-executable within the Fedora Linux context
  (note: this means that the files cannot run on their own, not that
  they are just `+chmod -x+`)

* The files must not be libraries, within the Fedora Linux context

* The files must be standalone, not embedded in executable or library
  code (within the Fedora Linux context)

* The files must be necessary for the functionality of open source
  code being included in Fedora Linux or to enable Fedora Linux to
  boot on a specific device, where no other reliable and supported
  mechanisms exist

== Not-allowed Licenses

Any license that does not meet the criteria above for allowed licenses
(including allowed for a specific category) is not-allowed.

NOTE: In extremely rare cases we may have reason to designate a license as
not-allowed even though it might meet the criteria for the relevant
category. One such case involved a license that was based on the
Apache License 2.0 and which was named "Modified Apache License 2.0",
(apparently) without permission from the Apache Software Foundation,
which has said that derivatives of the Apache License cannot have
"Apache" in their name.

== Changes in License Status

While Fedora aims for stability in its license classifications, they
have never been set in stone. Fedora's treatment of a particular
license or license type may change as community standards and
expectations around acceptable licensing evolve or as new issues and
concerns come to light. This is most likely to involve an allowed
license being reclassified as not-allowed. When this sort of
reclassification is done, we will try to minimize disruption,
including by making use of appropriate usage exceptions for licenses
no longer classified as allowed. A good example is the
reclassification of CC0 as being allowed only for content.

== Background to Fedora licensing policy

Over several decades, norms concerning acceptable standards for
software (and other content) licensing developed out of communities
associated with free software and open source projects. Fedora builds
upon and contributes to the development of this community
tradition. Most of the philosophical and practical groundwork for
Fedora's policies on licensing was developed over many years by
https://fedoraproject.org/wiki/User:Spot[Tom 'spot' Callaway], often
working in collaboration with members of Red Hat's legal team.

The two most influential efforts to distill FOSS licensing norms are
the Free Software Foundation's maintenance and interpretation of the
https://www.gnu.org/philosophy/free-sw.en.html#fs-definition[Free
Software Definition], and the Open Source Initiative's maintenance and
interpretation of the https://opensource.org/osd[Open Source
Definition]. For much of its earlier history, Fedora tended to regard
the FSF's interpretation as highly authoritative, while viewing the
OSI's license review decisions with greater skepticism. However, even
at the high point of that period of skepticism, Fedora treated the OSD
and OSI decisions as one source of persuasive authority. Fedora has
also sometimes taken into account the decisions of other major
community Linux distributions and other important community efforts to
define and apply software freedom-related legal norms. Fedora's
license approval decisions are both principled and pragmatic.

Out of necessity, Fedora has passed judgment on many licenses never
considered by the FSF or the OSI. In a small number of cases, Fedora
has disagreed with decisions of the FSF and OSI regarding whether
particular licenses are FOSS. Over time, Fedora has built up an
informal body of interpretation and policymaking regarding free/open
licensing which has itself influenced the larger FOSS community
outside of Fedora.
